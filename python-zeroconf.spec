%global pypi_name zeroconf
%global _description \
Pure Python Multicast DNS Service Discovery Library (Bonjour/Avahi compatible)

Summary:        Pure Python Multicast DNS Service Discovery Library (Bonjour/Avahi compatible)
Name:           python-zeroconf
Version:        0.120.0
Release:        1%{?dist}
License:        LGPL-2.0-only and LGPL-2.1-only
URL:            https://github.com/jstasiak/python-zeroconf
Source0:        %pypi_source

BuildRequires:  python3-pytest python3-pytest-asyncio python3-cython

BuildArch:      noarch

%description %{_description}

%package -n python3-%{pypi_name}
Summary:        %{summary}

%description -n python3-%{pypi_name} %{_description}

%prep
%autosetup -n %{pypi_name}-%{version}

sed -i 's/poetry-core>=1.5.2/poetry-core/' pyproject.toml
sed -Ei 's/--cov(-|=)[^ "]+//g' pyproject.toml

%generate_buildrequires
%pyproject_buildrequires

%build
%pyproject_wheel

%install
%pyproject_install
%pyproject_save_files %{pypi_name}

%check
# disable IPv6 tests due to failing in Koji/mock
%pytest -v -k "not test_sending_unicast and not test_integration_with_listener_ipv6"

%files -n python3-%{pypi_name} -f %{pyproject_files}
%doc README.rst

%changelog
* Fri Nov 10 2023 Upgrade Robot <upbot@opencloudos.org> - 0.120.0-1
- Upgrade to version 0.120.0

* Sat Oct 7 2023 Shuo Wang <abushwang@tencent.com> - 0.102.0-1
- update to 0.102.0

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.39.4-4
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.39.4-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.39.4-2
- Rebuilt for OpenCloudOS Stream 23.05

* Mon Apr 17 2023 kianli <kianli@tencent.com> - 0.39.4-1
- initial build
